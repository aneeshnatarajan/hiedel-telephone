import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CostCheckerComponent } from './cost-checker/cost-checker.component';
import { FormsModule } from '@angular/forms';
import { GetConnectionsService } from './services/get-connections.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CostCheckerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [GetConnectionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
