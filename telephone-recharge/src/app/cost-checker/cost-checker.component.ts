import { Component, OnInit } from '@angular/core';
import { GetConnectionsService } from '../services/get-connections.service';

@Component({
  selector: 'app-cost-checker',
  templateUrl: './cost-checker.component.html',
  styleUrls: ['./cost-checker.component.scss']
})
export class CostCheckerComponent implements OnInit {
 connections:any;
 operators=[];
 selected:string;
 availableConnections =[];
 phone: string;
 bestOperator:any;
 isDataAvailable:boolean;


  constructor(private connection: GetConnectionsService) { }

  ngOnInit() {
    this.getOPeratorsJSON();
    this.isDataAvailable = true;
  }
  getOPeratorsJSON(){
        this.connection.getConnections().subscribe((res) => {
      this.connections = res;
      this.connections.forEach((element) => {
          if (!this.operators.includes(element.operator)) {
              this.operators.push(element.operator);
          }
      });
     this.getListings(this.operators[0]);
  });
  }

  getListings(operatorName) {
    this.selected = operatorName;
    this.availableConnections = this.connections.filter((element) => {
        return element.operator === operatorName;
    });
}

getBestOperator() {
  let answer: any;
  const phoneNumber = this.phone.replace(/[^0-9]/g, '');
  this.connections.forEach((connection) => {
      if (phoneNumber.indexOf(connection.prefix) === 0) {
          if (!answer) {
              answer = connection;
          } else {
              if (answer.operator === connection.operator) {
                  if (connection.prefix.length > answer.prefix.length) {
                      answer = connection;
                  }
              } else if (answer.cost > connection.cost) {
                  answer = connection;
              }
          }
      }
  });
  if(answer){
    this.isDataAvailable = true;
    this.bestOperator = answer;
  }else{
    this.isDataAvailable = false;
    this.bestOperator = undefined;
  }
  
}
}
