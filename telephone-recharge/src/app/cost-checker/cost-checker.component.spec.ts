import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostCheckerComponent } from './cost-checker.component';

describe('CostCheckerComponent', () => {
  let component: CostCheckerComponent;
  let fixture: ComponentFixture<CostCheckerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostCheckerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostCheckerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
